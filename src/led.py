'''
Created on 01-May-2017
@author: bradan lane studio

'''

import logging
from time import sleep
from threadclass import ThreadClass
import RPi.GPIO as GPIO

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202, W0201


# -----------------------------------------------------------------------------------------------
# LED Indicators class (uses GPIO)
# -----------------------------------------------------------------------------------------------

class LEDIndicator(ThreadClass):
    def init(self, **kwargs):
        self.gpio_pin = 0
        self.illuminate = False
        logging.debug("entering LEDIndicator init()")
        if (kwargs is not None) and ('led' in kwargs):
            self.gpio_pin = kwargs.get('led', 0)
        if not self.gpio_pin:
            logging.warning("no GPIO pin assigned to LED")
        logging.debug("GPIO pin {:02d} assigned to LED".format(self.gpio_pin))
        GPIO.setmode(GPIO.BOARD)
        if self.gpio_pin:
            GPIO.setup(self.gpio_pin, GPIO.OUT)
        logging.debug("exiting LEDIndicator init()")
        return

    def term(self):
        logging.debug("LEDIndicator term()")
        self.turn_off()
        self.gpio_pin = 0
        #GPIO.cleanup()
        return

    def work(self):
        # simple alternating flasher thread on the shutter LED
        # used to indicate we are busy doing stuff
        self.blinks(0.5, 0.5, 1)
        # if LED was re-enabled during blinking, end in an 'on' state
        if self.illuminate:
            self.turn_on()
        return

    def enable(self):
        logging.debug("LEDIndicator enable()")
        self.illuminate = True
        self.turn_on()

    def disable(self):
        logging.debug("LEDIndicator disable()")
        self.illuminate = False
        self.turn_off()

    def turn_on(self):
        logging.debug("LEDIndicator turn_on()")
        if self.gpio_pin:
            GPIO.output(self.gpio_pin, GPIO.HIGH)

    def turn_off(self):
        logging.debug("LEDIndicator turn_off()")
        if self.gpio_pin:
            GPIO.output(self.gpio_pin, GPIO.LOW)

    def blink(self, on_time):
        logging.debug("LEDIndicator blink()")
        self.turn_on()
        sleep(on_time)
        self.turn_off()

    def blinks(self, on_time, off_time, count):
        logging.debug("LEDIndicator blinks()")
        self.turn_off() # just in case
        for _ in range(0, count):
            self.blink(on_time)
            sleep(off_time)

    def blink_code_once(self, num):
        logging.debug("LEDIndicator blink_code_once()")
        self.blinks(0.1, 0.2, num)
        sleep(1.5)

    def blink_code(self, num):
        logging.debug("LEDIndicator blink_code()")
        self.turn_off() # just in case
        # grab some attention
        self.blinks(0.5, 0.5, 4)
        # indicate code
        for _ in range(0, 4):
            self.blink_code_once(num)
        return

    def countdown(self, seconds):
        logging.debug("LEDIndicator countdown()")
        self.turn_off() # just in case
        for _ in range(0, (seconds-1)):
            self.turn_on()
            sleep(0.40)
            self.turn_off()
            sleep(0.60)
        for _ in range(0, 5):
            self.turn_on()
            sleep(0.1)
            self.turn_off()
            sleep(0.1)
        return
