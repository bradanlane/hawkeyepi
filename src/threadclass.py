'''
Created on 01-May-2017
@author: bradan lane studio

'''

import logging
import threading

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202


# -----------------------------------------------------------------------------------------------
# General Thread Class (uses GPIO)
# -----------------------------------------------------------------------------------------------

# Coding:
#     class MyClass(ThreadClass):
#     implement:
#         init(self)
#         term(self)
#         work(self)
# Usage:
#     my_obj = MyClass()
#     my_obj.start()
#     my_obj.trigger()
#     ...
#     optional: my_obj.trigger(False)
#     my_obj.stop()


class ThreadClass(object):
    def __init__(self, **kwargs):
        logging.debug("entering ThreadClass __init__()")
        self.__terminate = False
        self.__is_running = False
        self.__trigger = threading.Event()
        self.__thread = threading.Thread(target=self.__worker)

        self.init(**kwargs)
        logging.debug("exiting ThreadClass __init__()")
        # return

    '''
    Implement the next three methods
    '''

    def init(self, **kwargs):
        logging.debug("ThreadClass default init()")
        # implemented by subclass
        return

    def term(self):
        logging.debug("ThreadClass default term()")
        # implemented by subclass
        return

    def work(self):
        logging.debug("ThreadClass default work()")
        # implemented by subclass
        return


    '''
    Use the next three methods
    '''

    def start(self):
        logging.debug("entering ThreadClass start()")
        if not self.__is_running:
            self.__is_running = True
            self.__thread.start()
        logging.debug("exiting ThreadClass start()")
        return

    def trigger(self, active=True):
        logging.debug("ThreadClass trigger({})".format(active))
        if active:
            self.__trigger.set()
        else:
            self.__trigger.clear()
        return

    def stop(self):
        logging.debug("entering ThreadClass stop()")
        # this will end the flasher thread
        if self.__is_running:
            self.__is_running = False
            self.__trigger.clear()
            self.__terminate = True
            self.term()
            #sleep(1) # give time for threads to complete
        logging.debug("exiting ThreadClass stop()")
        return

    '''
    Internal methods
    '''

    def __worker(self):
        logging.debug('entering ThreadClass __worker()')
        # simple alternating flasher thread on the shutter LED
        # used to indicate we are busy doing stuff
        while not self.__terminate:
            self.__trigger.wait(0.2)
            if self.__trigger.is_set():
                self.work()
        logging.debug('exiting ThreadClass __worker()')
        return

