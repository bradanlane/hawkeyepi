'''
Created on 01-May-2017
@author: bradan lane studio

'''

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202
import os
import signal
from time import sleep
from fractions import Fraction
import time
import logging

# hardware IO
import RPi.GPIO as GPIO

import constants

'''
settings-mode uses the white flash button and the red shutter button
the white button long press to enter/exit settings-mode
the white button short press cycles through the settings
the red button cycles through the values of a setting

the settings audio clips are referenced from the AUDIO_SETTINGS array
the values are mapped by two array-of-arrays
PROGMODE_VALUES represents the piCamera API values
AUDIO_VALUES represents the audio clips

arrays use zero-based indexing
'''

class HawkeyePiProgrammer:
    def __init__(self, cfg, cam, function_led, value_led):
        logging.debug("entering Programmer __init__()")
        self.cfg = cfg
        self.cam = cam
        self.function_led = function_led
        self.value_led = value_led
        self.ready = False
        self.programming = False
        self.busy = False
        self.settings_index = 0
        self.values_index = 0
        logging.debug("exiting Programmer __init__()")
        #return

    def start(self):
        logging.debug('programmer start()')
        if self.cfg and self.cam and self.cam.camera and self.function_led and self.value_led:
            self.ready = True
            logging.debug('programmer ready')

    def stop(self):
        logging.debug('programmer stop()')
        self.ready = False



    def __get_setting_value_index(self, setting_index):
        if not self.ready: return -1

        # PROGMODE_LAST is defined in constrants.py and had better be 5
        if (setting_index < 0) or (setting_index > constants.PROGMODE_LAST):
            setting_index = 0

        if setting_index == 0:
            val = self.cfg.getboolean('general', 'print')
        elif setting_index == 1:
            val = self.cfg.getboolean('general', 'photobooth')
        elif setting_index == 2:
            val = self.cfg.get('camera', 'awb')
        elif setting_index == 3:
            val = self.cfg.get('camera', 'metering')
        elif setting_index == 4:
            val = self.cfg.get('camera', 'exposure')
        elif setting_index == 5:
            val = self.cfg.get('camera', 'iso')
        elif setting_index == 6:
            val = self.cfg.getboolean('general', 'help')
        elif setting_index == 7:
            val = self.cfg.getboolean('general', 'audio')

        choices = constants.PROGMODE_VALUES[setting_index]
        try:
            choice = choices.index(val)
        except ValueError:
            # if the actual value is not one of our available values we need to make an update
            # we will default back to the first option
            choice = self.__set_setting_value_by_index(setting_index, 0)
        return choice


    def __set_setting_value_by_index(self, setting_index, value_index):
        if not self.ready: return

        # PROGMODE_LAST is defined in constrants.py and had better be 5
        if (setting_index < 0) or (setting_index > constants.PROGMODE_LAST):
            setting_index = 0

        choices = constants.PROGMODE_VALUES[setting_index]
        if value_index >= len(choices):
            value_index = 0
        val = choices[value_index]

        if setting_index == 0:
            self.cfg.set('general', 'print', str(val))
        elif setting_index == 1:
            self.cfg.set('general', 'photobooth', str(val))
        elif setting_index == 2:
            self.cfg.set('camera', 'awb', val)
            self.cam.camera.awb_mode = val
        elif setting_index == 3:
            self.cfg.set('camera', 'metering', val)
            self.cam.camera.meter_mode = val
        elif setting_index == 4:
            self.cfg.set('camera', 'exposure', val)
            self.cam.camera.exposure_mode = val
            # we reduce the frame rate in night exposure mode
            if val == "night":
                self.cam.camera.framerate = Fraction(1, 6) # this means our slowest shutter is 6 seconds
            else:
                self.cam.camera.framerate = 15 # this is our default and means our slowest shutter is 1/15 second
        elif setting_index == 5:
            self.cfg.set('camera', 'iso', str(val))
            self.cam.camera.iso = val
        elif setting_index == 6:
            self.cfg.set('general', 'help', str(val))
        elif setting_index == 7:
            self.cfg.set('general', 'audio', str(val))

        self.cfg.update_config()
        logging.debug("changed mode #{:d} to {}".format(setting_index, val))
        return value_index

    def __next_value_index(self):
        logging.debug('next_value_index()')
        #sleep(0.5)
        self.values_index = self.__set_setting_value_by_index(self.settings_index, self.values_index+1)
        constants.PlayAudioMessage(constants.AUDIO_SETTINGS[self.settings_index], True)
        constants.PlayAudioMessage(constants.AUDIO_VALUES[self.settings_index][self.values_index], True)
        return

    def __next_setting_index(self):
        self.function_led.disable()
        self.settings_index += 1
        if self.settings_index > constants.PROGMODE_LAST:
            self.settings_index = 0
        constants.PlayAudioMessage(constants.AUDIO_SETTINGS[self.settings_index], True)
        self.values_index = self.__get_setting_value_index(self.settings_index)
        constants.PlayAudioMessage(constants.AUDIO_VALUES[self.settings_index][self.values_index], True)
        self.function_led.enable()
        logging.debug("programming mode is {:d} and value is {:d}".format(self.settings_index, self.values_index))


    def __start_programming(self):
        logging.debug("start_programming()")
        # enter programming mode
        self.programming = True
        self.settings_index = -1 # initial to before first setting
        # let next_setting_index() do the rest of the work
        self.__next_setting_index()
        return

    def __stop_programming(self):
        logging.debug("stop_programming()")
        constants.PlayAudioMessage(constants.AUDIO_PROGRAMMER_EXIT, True)
        self.programming = False
        self.settings_index = 0
        # indicate exiting programming mode
        #self.function_led.blinks(0.5, 0.5, 4)
        # restore LEDs into a known state
        self.function_led.disable()
        self.value_led.enable() # reenable the shutter button LED




    def shutter_button(self):
        logging.debug('entering shutter_button()')
        # default behavior is to take a picture
        if self.programming:
            self.__next_value_index()
        else:
            self.cam.take_pictures()
        logging.debug('exiting shutter_button()')
        return


    def program_button(self):
        logging.debug('before program_button()')
        if not self.ready: return
        logging.debug('entering program_button()')
        if self.busy:
            # rather than extend the GPIO bouncetime and slow every action down
            # we just deal with the cases of the very/long press
            logging.debug('exiting an attempted re-entry')
            return

        self.busy = True

        '''
        camera functions are available front the front button.
        a 'press' is any length
        a 'short press' is less that 1 sec
        a 'long press' is 3 sec
        a 'very long press' is 8 sec
        config restore is triggered by a very long press of the front button.
        programming mode is triggered by a long press of the front button.
        in programming mode, a press of the front button cycles modes.
        in programming mode, a long press of the front button exits mode.
        in programming mode, a press of the shutter button cycles values.
        the button LED indicates initial mode/value and changed mode/value.
        PROGMODE_PRINTER    = 1
            0=printer_on:   1=false 2=true
            1=boothmode_on: 1=false 2=true
            2=awb:          1=auto 2=sunny 3=cloudy 4=florecent 5=incandescent, 6=off/custom
            3=metering:     1=spot 2=average 3=backlit
            4=exposure:     1=auto 2=sports 3=fireworks 4=night 5=off
            5=iso:          auto, 100, 200, 400, 800, 1600
            6=help          off, on
        '''

        # put LEDs into a known state
        self.function_led.disable()
        self.value_led.disable()

        time_pressed = time.time()
        while GPIO.input(constants.PROGRAM_BUTTON) == 0: # Wait for the button up
            pass
        duration = time.time() - time_pressed    # How long was the button down?
        #duration =  duration.seconds + duration.microseconds/1E6 # get duration as float seconds
        logging.debug("program_button time {:f}".format(duration))
        #sleep(0.5) # give the user a moment to remove their finger so they can see the button LED

        if not self.programming:
            if duration >= constants.PROGMODE_VLONGPRESS:
                logging.debug("program_button very long press")
                if self.cfg.restore_backup():
                    #signal interupt (CTRL+C) which will be caught by our main handler
                    os.kill(os.getpid(), signal.SIGINT)
                else:
                    # the restore failed
                    # we should flash an indication of some sort
                    logging.warning("failed to restore backup configuration")

            elif duration >= constants.PROGMODE_LONGPRESS:
                logging.debug("program_button long press")
                # help system - settings introduction and instructions
                if self.cfg.getboolean('general', 'help'):
                    constants.PlayAudioMessage(constants.AUDIO_PROGRAMMER_INSTRUCTIONS, True)
                else:
                    constants.PlayAudioMessage(constants.AUDIO_PROGRAMMER_ENTER, True)
                self.__start_programming()
            else:
                logging.debug("program_button short press, but we are not yet in programming mode")
                # help system - camera introduction and instructions
                if self.cfg.getboolean('general', 'help'):
                    constants.PlayAudioMessage(constants.AUDIO_INSTRUCTIONS, True)
                else:
                    constants.PlayAudioMessage(constants.AUDIO_NAME, True)
                self.value_led.enable() # reenable the shutter button LED

        else: # already in programming mode
            logging.debug("program_button press while in programming mode")
            if duration >= constants.PROGMODE_LONGPRESS:
                self.__stop_programming()
            else:
                self.__next_setting_index()
        logging.debug("exiting program_button()")
        self.busy = False
        return

