'''
Created on 01-May-2017
@author: bradan lane studio

'''

# cSpell:disable
# pylint: disable=I0011, C0103, C0111, C0321, C0301, C0326, W0613, W1202

import os

SERIAL_SPEEDS       = [4800,9600,19200,38400,57600,115200]
ISO_SPEEDS          = [0,100,200,320,400,500,640,800,1600]
SHUTTER_SPEEDS      = [0,15,20,30,60,90,125,160,250,320,500,640,1000]
# these will get set if the camera is initialized
AWB_MODES           = [""]
EXPOSURE_MODES      = [""]
METER_MODES         = [""]

# define the root of our web server files - this needs to be an absolute path
PROJECTROOT         = "/home/pi/hawkeyepi/"
WEBROOT             = PROJECTROOT + "www/"
SRCROOT             = PROJECTROOT + "src/"
AUDIO_FOLDER        = SRCROOT + "audio/"

TEST_FOLDER         = "/media/usb0"
FILE_FOLDER         = "/media/usb0/images/"
THUMB_FOLDER        = "/media/usb0/images/thumbs/"
PRINT_FOLDER        = "/media/usb0/images/print/"


# hardware GPIO PINS
SHUTTER_BUTTON      = 12
#GND                = 14
SHUTTER_LED         = 16
FRONT_LED           = 18
#GND                = 20
PROGRAM_BUTTON      = 22



PROGMODE_SHORTPRESS = 0.5 # less than 1 sec
PROGMODE_LONGPRESS  = 2 # 3 sec up to next length
PROGMODE_VLONGPRESS = 8 # 8 sec or more

CAMERA_WIDTH_V1        = 2592
CAMERA_HEIGHT_V1       = 1944
CAMERA_WIDTH_V2        = 3280
CAMERA_HEIGHT_V2       = 2464

CAMERA_WIDTH           = CAMERA_WIDTH_V2
CAMERA_HEIGHT          = CAMERA_HEIGHT_V2

PRINTER_WIDTH       = 512
PRINTER_HEIGHT      = 384


MSGOK               = 0
MSGNOUSB            = 1
MSGUSBFOLDERS       = 2
MSGCFGREADERROR     = 3
MSGCFGWRITEERROR    = 4
MSGCFGPARAM         = 5
MSGCAMCREATE        = 6
MSGCAMSETUP         = 7
MSPRINTCREATE       = 8
MSPRINTNOPAPER      = 9
MSGBAUDCHANGE       = 10
MSGPORTCHANGE       = 11
MSGBADCMD           = 12
MSGUNKNOWN          = 13

# cSpell:enable

HAWKEYE_PI_MESSAGES = [ \
"", \
"general: unable to access usb device", \
"general: unable to access usb folders", \
"config: unable to read configuration file", \
"config: unable to write configuration file", \
"config: missing parameter", \
"camera: unable to create", \
"camera: unable to setup", \
"printer: unable to create", \
"printer: out of paper", \
"printer: reboot required after baudrate change", \
"web: reboot required after port change", \
"web: unrecognized command", \
"unknown error" \
]

# cSpell:disable

def GetMessageText(code):
    if (code < MSGOK) or (code >= MSGUNKNOWN):
        code = MSGUNKNOWN
    return HAWKEYE_PI_MESSAGES[code]


'''
the audio files were generated using text-to-speach
using this online service:
http://oddcast.com/home/demos/tts/tts_example.php
the voice is 'Hugh (UK)' with no extra effects

to download the mp3 file, use the following instructions:
    after "say it"
    inspect page
    switch to "network" tab
    insure "all" is displayed
    scroll to bottom
    right click on gen.php?apt... 
    "open in new tab"
    save-to filename

The maximum length of text may be expanded by 'inspect' the HTML "enter Text" field and change the default of 180 characters.

audio is played using a system call to mpg123
pre-requisite: sudo apt-get install mpg123

test the audio files from command line:
mpg123 file_name.mp3 -q -g <volume 0..100> -k <start frame> -n <frames length>

usage from python:
os.system('<command line as above> &')
'''

AUDIO_COMMAND               = '/usr/bin/mpg123 -q -g 150 '

# to create audio lists, use comma delimited text

# TODO - an optimization to the code would be to only store the sound file name and an array of offsets

# AUDIO COUNT: 1 2 3 4 5 6 7 8 9 10
AUDIO_COUNT = [ \
'count.mp3 -n  24', \
'count.mp3 -k  12 -n  50', \
'count.mp3 -k  32 -n  76', \
'count.mp3 -k  54 -n  98', \
'count.mp3 -k  98 -n 128', \
'count.mp3 -k 128 -n 158', \
'count.mp3 -k 158 -n 188', \
'count.mp3 -k 188 -n 218', \
'count.mp3 -k 218 -n 246', \
'count.mp3 -k 246' \
]
AUDIO_COUNT_LAST = 9

# AUDIO OFFON: off on
AUDIO_OFFON = [ \
'offon.mp3 -n 20', \
'offon.mp3 -k 20' \
]
AUDIO_OFFON_LAST = 1

# AUDIO AWB: auto sunlight cloudy flourescent incandecent off
AUDIO_AWB = [ \
'awb.mp3 -n  28', \
'awb.mp3 -k  28 -n  60', \
'awb.mp3 -k  60 -n  90', \
'awb.mp3 -k  90 -n 120', \
'awb.mp3 -k 120 -n 170', \
'awb.mp3 -k 170' \
]
AUDIO_AWB_LAST = 5

# AUDIO METERING: spot average backlit
AUDIO_METERING = [ \
'metering.mp3 -n  30', \
'metering.mp3 -k 30 -n  60', \
'metering.mp3 -k 60' \
]
AUDIO_METERING_LAST = 2

# AUDIO EXPOSURE: auto sports fireworks beach snow night off
AUDIO_EXPOSURE = [ \
'exposure.mp3 -n  28', \
'exposure.mp3 -k  28 -n  58', \
'exposure.mp3 -k  58 -n  90', \
'exposure.mp3 -k  90 -n  120', \
'exposure.mp3 -k 120 -n  150', \
'exposure.mp3 -k 150 -n  180', \
'exposure.mp3 -k 180' \
]
AUDIO_EXPOSURE_LAST = 6

# AUDIO ISO: auto 100 200 400 800 1600
AUDIO_ISO = [ \
'iso.mp3 -n  28', \
'iso.mp3 -k  28 -n  60', \
'iso.mp3 -k  72 -n  90', \
'iso.mp3 -k  90 -n 130', \
'iso.mp3 -k 130 -n 170', \
'iso.mp3 -k 170' \
]
AUDIO_ISO_LAST = 5

# AUDIO OPTIONS: Printer, Photobooth, White Balance, Metering, Exposure, ISO, Help Messages, Shutter Audio
AUDIO_VALUES = [ AUDIO_OFFON, AUDIO_OFFON, AUDIO_AWB, AUDIO_METERING, AUDIO_EXPOSURE, AUDIO_ISO, AUDIO_OFFON, AUDIO_OFFON ]
AUDIO_SETTINGS = [ \
'settings.mp3 -n   24', \
'settings.mp3 -k  24 -n   68', \
'settings.mp3 -k  68 -n  108', \
'settings.mp3 -k 108 -n  138', \
'settings.mp3 -k 138 -n  180', \
'settings.mp3 -k 180 -n  210', \
'settings.mp3 -k 210 -n  260', \
'settings.mp3 -k 260' \
]
AUDIO_OPTIONS_LAST = 7 # (zero-based) currently this needs to be coordinated with programmer.py

# list of settings with actual piCamera values
PROGMODE_PRINTER    = [False, True]
PROGMODE_BOOTH      = [False, True]
PROGMODE_AWB        = ['auto', 'sunlight', 'cloudy', 'fluorescent', 'incandescent', 'off']
PROGMODE_METERING   = ['spot', 'average', 'backlit']
PROGMODE_EXPOSURE   = ['auto', 'sports', 'fireworks', 'beach', 'snow', 'night', 'off']
PROGMODE_ISO        = [0, 100, 200, 400, 800, 1600]
PROGMODE_HELP       = [False, True]
PROGMODE_AUDIO      = [False, True]
PROGMODE_LAST       = AUDIO_OPTIONS_LAST
PROGMODE_VALUES     = [ PROGMODE_PRINTER, PROGMODE_BOOTH, PROGMODE_AWB, PROGMODE_METERING, PROGMODE_EXPOSURE, PROGMODE_ISO, PROGMODE_HELP, PROGMODE_AUDIO ]


AUDIO_SHUTTER                 = 'shutterrelease.mp3'
AUDIO_READY                   = 'ready.mp3'
AUDIO_NAME                    = 'hawkeyepicamera.mp3'

# The Hawkeye Pi camera supports two modes - camera and photo booth. In camera mode, a photo is taken when the red shutter button is pressed. In photo booth mode, there is a delay after the shutter button is pressed to give the subject time to get into position. Then, a series of photos are taken with a 5 second delay between each. The white LED flashs to indicate the shutter delay. The front white button is used to enter settings mode. Press and hold the white button for 3 seconds to enter settings mode. Settings control the printer, camera mode, white balance, metering, exposure, ISO, and help messages. There is also a web interface with additional settings and to review photos.
AUDIO_INSTRUCTIONS            = 'instructions.mp3'
# You have entered settings mode. In settings mode you may turn the printer on or off, switch between camera mode and photobooth mode, change white balance, metering, exposure, ISO, and turn the help messages on or off. Press the white button to cycle through the settings. Press the red button to change the value of the current setting. Press and hold the white button for 3 seconds to exit settings mode.
AUDIO_PROGRAMMER_INSTRUCTIONS = 'programmer_instructions.mp3'
AUDIO_PROGRAMMER_ENTER = 'programmer_enter.mp3'
AUDIO_PROGRAMMER_EXIT = 'programmer_exit.mp3'

# used by autohotspotswapper
#AUDIO_WIFI                    = 'network_wifi.mp3'
#AUDIO_ACCESSPOINT             = 'network_accesspoint.mp3'



def PlayAudioMessage(msg, wait):
    if wait:
        os.system(AUDIO_COMMAND + AUDIO_FOLDER + msg + "> /dev/null 2>&1")
    else:
        os.system(AUDIO_COMMAND + AUDIO_FOLDER + msg + "> /dev/null 2>&1 &")
    return

