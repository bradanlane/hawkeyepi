'''
Created on 01-May-2017
@author: bradan lane studio

'''

import os
import logging
import configparser
import constants

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202, R0901

CFG_FILE = "/media/usb0/hawkeyepi.cfg"
BAK_FILE = "/media/usb0/hawkeyepi.bak"


class Configurator(configparser.ConfigParser):
    def __init__(self):
        self.status_code = 0
        self.status_message = ""
        self.filename = CFG_FILE
        self.backupname = BAK_FILE

        configparser.ConfigParser.__init__(self)
        self['DEFAULT'] = {
            'print': 'True', \
            'photobooth': 'True', \
            'help': 'True', \
            'audio': 'True', \
            'delay': '10', \
            'burstsize': '1', \
            'iso': '0', \
            'speed': '0', \
            'contrast': '0', \
            'brightness': '50', \
            'awb_red': '1.0', \
            'awb_blue': '1.0', \
            'awb': 'auto', \
            'exposure': 'auto', \
            'metering': 'matrix', \
            'rotation': '0', \
            'hflip': 'False', \
            'vflip': 'False', \
            'baudrate': '-1', \
            'dotprinttime': '0.025', \
            'darkness': '70', \
            'tagline': '', \
            'webport': '8888' \
            }

        # this loads the entire config file
        logging.debug("Loading config from {}".format(self.filename))
        # load the backup settings and then overwrite any changes from the primary settings
        self.read([self.backupname, self.filename])

        # prep config file in case it does not exist
        if not self.has_section('general'):
            self.add_section('general')
        if not self.has_section('camera'):
            self.add_section('camera')
        if not self.has_section('printer'):
            self.add_section('printer')
        # return


    def update_config(self):
        self.__write_config(self.filename)
        return


    def backup_config(self):
        logging.info("creating backup of configuration")
        tmp = self.backupname + "_tmp"
        err = constants.MSGOK
        self.__write_config(tmp)
        try:
            size = os.path.getsize(tmp)
        except OSError:
            err = constants.MSGCFGWRITEERROR
            logging.warning("failed to creat backup of configuration")
            size = 0
        if not size:
            err = constants.MSGCFGWRITEERROR
            logging.warning("invalid size to backup of configuration")
            return constants.MSGCFGWRITEERROR
        else:
            # we created a success backup, now make it real
            try:
                logging.debug("renaming {} to {}".format(tmp, self.backupname))
                os.remove(self.backupname)
                os.rename(tmp, self.backupname)
            except OSError:
                pass
        return err


    def restore_backup(self):
        contents = None
        with open(self.backupname) as filep:
            contents = filep.read()
            filep.close()
        if contents:
            with open(self.filename, 'w') as filep:
                filep.write(contents)
                filep.close()
                return True
        return False


    def __write_config(self, filename):
        logging.debug("Saving config to {}".format(filename))
        #self.remove_section('DEFAULT')
        #self._defaults = None # total hack to get rid of the DEFAULTS
        with open(filename, 'w') as cfgfilep:
            self.write(cfgfilep)
            cfgfilep.close()
        return
