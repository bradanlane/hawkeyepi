'''
Created on 01-May-2017
@author: bradan lane studio

'''

import os
import signal
import time
import logging
from fractions import Fraction
import base64
try:
    import cStringIO as io
except ImportError:
    import io

import constants

# we need the tornado web and web framework stuff
import tornado.httpserver
import tornado.template
import tornado.web
import tornado.websocket
import tornado.ioloop

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202, W0201


# -----------------------------------------------------------------------------------------------
# tornado webserver handlers
# provide the simple UI for viewing/printing/deleting photos as well aschanging camera settings
# -----------------------------------------------------------------------------------------------

class StaticFileHandler(tornado.web.StaticFileHandler):
    def write(self, chunk):
        #logging.debug("WebServer StaticFileHandler write()")
        super(StaticFileHandler, self).write(chunk)
        self.flush()


class IndexHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ['GET']

    def get(self, path):
        logging.debug("WebServer IndexHandler get()")
        # regardless of the resource request, respond with the top level index file
        page = constants.WEBROOT + 'index.html'
        if os.path.exists(page):
            content = []
            files = os.listdir(constants.THUMB_FOLDER)
            files.sort(reverse=True)
            # IMPORTANT: the web page assumes a symlink from www/photos to the USB folder
            # command> cd hawkeyepi/www; ln -s /media/usb0/images photos
            for file in files:
                if file.endswith(".jpg"):
                    content.append(file)
            self.render(page, photos=content, folder='thumbs')
            return
        return tornado.web.HTTPError(404)


class PrintHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ['GET']

    def get(self, path):
        logging.debug("WebServer PrintHandler get()")
        # regardless of the resource request, respond with the top level index file
        page = constants.WEBROOT + 'index.html'
        if os.path.exists(page):
            content = []
            files = os.listdir(constants.PRINT_FOLDER)
            files.sort(reverse=True)
            for file in files:
                if file.endswith(".jpg"):
                    content.append(file)
            self.render(page, photos=content, folder='print')
            return
        return tornado.web.HTTPError(404)


class CommandHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ['GET']

    def initialize(self, cfg, cam):
        logging.debug("WebServer CommandHandler initialize()")
        self.cam = cam # the hawkeypicamera object which contains the picamera object
        self.cfg = cfg
        return

    def get(self, path):
        logging.debug("WebServer CommandHandler get()")
        refresh = False
        home = False
        err = self.cam.errcode
        self.cam.errcode = constants.MSGOK

        # get necessary configuration
        command = self.get_query_argument("cmd", "status")
        setting = self.get_query_argument("val", None)

        if command == "toggleprint":
            val = self.cfg.getboolean('general', 'print')
            val = not val
            self.cfg.set('general', 'print', str(val))
            refresh = True
        elif command == "togglebooth":
            val = self.cfg.getboolean('general', 'photobooth')
            val = not val
            self.cfg.set('general', 'photobooth', str(val))
            refresh = True
        elif command == "togglehflip":
            val = self.cfg.getboolean('camera', 'hflip')
            val = not val
            if self.cam and self.cam.camera:
                self.cam.camera.hflip = val
            self.cfg.set('camera', 'hflip', str(val))
            refresh = True
        elif command == "togglevflip":
            val = self.cfg.getboolean('camera', 'vflip')
            val = not val
            if self.cam and self.cam.camera:
                self.cam.camera.vflip = val
            self.cfg.set('camera', 'vflip', str(val))
            refresh = True
        elif "rotate" in command:
            val = self.cfg.getint('camera', 'rotation')
            if command.endswith("up"):
                val += 90
                if val > 360:
                    val -= 360
            else: # down
                val -= 90
                if val < 0:
                    val += 360
            if self.cam and self.cam.camera:
                self.cam.camera.rotation = val
            self.cfg.set('camera', 'rotation', str(val))
            refresh = True
        elif "delay" in command:
            val = self.cfg.getint('general', 'delay')
            if command.endswith("up"):
                val += 1
                if val > 30:
                    val = 30
            else: # down
                val -= 1
                if val < 0:
                    val = 0
            self.cfg.set('general', 'delay', str(val))
            refresh = True
        elif "burst" in command:
            val = self.cfg.getint('general', 'burstsize')
            if command.endswith("up"):
                val += 1
                if val > 6:
                    val = 6
            else: # down
                val -= 1
                if val < 1:
                    val = 1
            self.cfg.set('general', 'burstsize', str(val))
            refresh = True
        elif command == "tagline":
            self.cfg.set('printer', 'tagline', setting)
            refresh = True
        elif "awb_" in command: #awb_red or awb_blue
            val = float(setting)
            if val <= 0.0:
                val = 0.0
            if self.cam and self.cam.camera:
                red, blue = self.cam.camera.awb_gains
            if command.endswith("red"):
                self.cfg.set('camera', 'awb_red', str(val))
                red = val
            else:
                self.cfg.set('camera', 'awb_blue', str(val))
                blue = val
            if self.cam and self.cam.camera:
                self.cam.camera.awb_gains = (red, blue)
            time.sleep(0.5) # need to sleep a moment to let the new setting to get established
            refresh = True
        elif command == "awb":
            if setting is None:
                setting = "auto"
            if self.cam and self.cam.camera:
                self.cam.camera.awb_mode = setting
            self.cfg.set('camera', 'awb', setting)
            refresh = True
        elif command == "speed":
            val = int(setting)
            if val <= 0:
                val = 0
            if self.cam and self.cam.camera:
                # shutter speed UI uses traditional integer values for fractions of a second
                # these need to be converted to microseconds
                if val == 0:
                    self.cam.camera.shutter_speed = 0
                else:
                    self.cam.camera.shutter_speed = round(1000000 / val)
            self.cfg.set('camera', 'speed', str(val))
            time.sleep(0.5) # need to sleep a moment to let the new setting to get established
            refresh = True
        elif command == "exposure":
            if setting is None:
                setting = "auto"
            if self.cam and self.cam.camera:
                self.cam.camera.exposure_mode = setting
                if (setting == "night") or (setting == "verylong") or (setting == "fireworks"):
                    # this really stretches the time to take a photo
                    self.cam.camera.framerate = Fraction(1, 6) # this means our slowest shutter is 6 seconds
                else:
                    self.cam.camera.framerate = 15 # this is our default and means our slowest shutter is 1/15 second
            self.cfg.set('camera', 'exposure', setting)
            refresh = True
        elif command == "metering":
            if setting is None:
                setting = "average"
            if self.cam and self.cam.camera:
                self.cam.camera.meter_mode = setting
            self.cfg.set('camera', 'metering', setting)
            refresh = True
        elif command == "tagline":
            self.cfg.set('printer', 'tagline', setting)
            refresh = True
        elif command == "iso":
            val = int(setting)
            if val <= 0:
                val = 0
            if self.cam and self.cam.camera:
                self.cam.camera.iso = val
            self.cfg.set('camera', 'iso', str(val))
            refresh = True
        elif "brightness" in command:
            val = self.cfg.getint('camera', 'brightness')
            if command.endswith("up"):
                val += 5
                if val > 100:
                    val = 100
            else: # down
                val -= 5
                if val < 5:
                    val = 5
            if self.cam and self.cam.camera:
                self.cam.camera.brightness = val
            self.cfg.set('camera', 'brightness', str(val))
            refresh = True
        elif "contrast" in command:
            val = self.cfg.getint('camera', 'contrast')
            if command.endswith("up"):
                val += 5
                if val > 100:
                    val = 100
            else: # down
                val -= 5
                if val < -100:
                    val = -100
            if self.cam and self.cam.camera:
                self.cam.camera.contrast = val
            self.cfg.set('camera', 'contrast', str(val))
            refresh = True
        elif "darkness" in command:
            val = self.cfg.getint('printer', 'darkness')
            if command.endswith("up"):
                val += 5
                if val > 200:
                    val = 200
            else: # down
                val -= 5
                if val < 5:
                    val = 5
            if self.cam and self.cam.printer:
                self.cam.printer.hdwr.begin(val)
            self.cfg.set('printer', 'darkness', str(val))
            refresh = True
        elif command == "printspeed":
            val = float(setting)
            # could be risky but it admin stuff so its all a risk
            if (val > 0.0) and self.cam and self.cam.printer:
                self.cam.printer.hdwr.dotPrintTime = val
            self.cfg.set('printer', 'dotprinttime', str(val))
            refresh = True
        elif command == "baudrate":
            val = int(setting)
            if val <= 0:
                val = 9600
            self.cfg.set('printer', 'baudrate', str(val))
            self.cam.errcode = constants.MSGBAUDCHANGE
            refresh = True
        elif command == "webport":
            val = int(setting)
            self.cfg.set('general', 'webport', str(val))
            self.cam.errcode = constants.MSGPORTCHANGE
            refresh = True
        elif command == "print":
            if self.cam and self.cam.printer:
                self.cam.printer.print_photo(setting) # print a named image file
            home = True
        elif command == "delete":
            if self.cam:
                self.cam.delete_photo(setting) # delete a named image file and thumbnail
            home = True
        elif command == "togglehelp":
            val = self.cfg.getboolean('general', 'help')
            val = not val
            self.cfg.set('general', 'help', str(val))
            refresh = True
        elif command == "backup":
            if self.cfg:
                self.cam.errcode = self.cfg.backup_config()
            refresh = True
        elif command == "restore":
            if self.cfg:
                self.cam.errcode = self.cfg.restore_config()
            refresh = True
        elif command == "exit":
            self.stop_server()
            refresh = True
        elif command == "reboot":
            self.reboot_system(None)
            refresh = True
        elif command == "shutdown":
            self.halt_system(None)
            refresh = True
        elif command == "forceap":
            self.force_access_point()
            refresh = True
        elif command == "status":
            page = constants.WEBROOT + 'status.html'
            # decide is there are any error to report
            if (not self.cam) or (not self.cam.camera):
                err = constants.MSGCAMCREATE
            if self.cam and self.cam.errcode:
                err = self.cam.errcode
            # else we go with any error we already have

            local_awb_mode = self.cfg.get('camera', 'awb')
            if (local_awb_mode == "off") or (not self.cam) or (not self.cam.camera):
                local_awb_red = self.cfg.getfloat('camera', 'awb_red')
                local_awb_blue = self.cfg.getfloat('camera', 'awb_blue')
            else:
                local_awb_red, local_awb_blue = self.cam.camera.awb_gains
                local_awb_red = round(local_awb_red+0.0, 3) # converts fraction to float
                local_awb_blue = round(local_awb_blue+0.0, 3) # converts fraction to float
            local_speed = round(1000000 / max (self.cam.camera.exposure_speed, 1))

            self.render(page, \
            message=constants.GetMessageText(err), \
            baud_choices=constants.SERIAL_SPEEDS, \
            awb_choices=constants.AWB_MODES, \
            meter_choices=constants.METER_MODES, \
            exposure_choices=constants.EXPOSURE_MODES, \
            speed_choices=constants.SHUTTER_SPEEDS, \
            iso_choices=constants.ISO_SPEEDS, \
            iso=self.cfg.getint('camera', 'iso'), \
            contrast=self.cfg.getint('camera', 'contrast'), \
            brightness=self.cfg.getint('camera', 'brightness'), \
            awb_mode=local_awb_mode, \
            meter_mode=self.cfg.get('camera', 'metering'), \
            exposure_mode=self.cfg.get('camera', 'exposure'), \
            speed=self.cfg.getint('camera', 'speed'), 
            shutter_speed=local_speed, \
            awb_red=local_awb_red, \
            awb_blue=local_awb_blue, \
            rotation=self.cfg.getint('camera', 'rotation'), \
            hflip=self.cfg.getboolean('camera', 'hflip'), \
            vflip=self.cfg.getboolean('camera', 'vflip'), \
            baudrate=self.cfg.getint('printer', 'baudrate'), \
            dotprinttime=self.cfg.getfloat('printer', 'dotprinttime'), \
            darkness=self.cfg.getint('printer', 'darkness'), \
            tagline=self.cfg.get('printer', 'tagline'), \
            booth=self.cfg.getboolean('general', 'photobooth'), \
            help=self.cfg.getboolean('general', 'help'), \
            printer=self.cfg.getboolean('general', 'print'), \
            delay=self.cfg.getint('general', 'delay'), \
            burstsize=self.cfg.getint('general', 'burstsize'), \
            webport=self.cfg.getint('general', 'webport'))
        else:
            self.cam.errcode = constants.MSGBADCMD
            refresh = True

        if home:
            self.redirect("/")
        if refresh:
            if self.cfg:
                self.cfg.update_config()
            self.redirect("/api")
        return # tornado.web.HTTPError(404)

    def force_access_point(self):
        logging.warning("Switching networking")
        os.system("sudo /home/pi/hawkeyepi/sysfiles/force-ap")

    def halt_system(self, channel):
        logging.warning("Halting system")
        os.system("sudo halt")

    def reboot_system(self, channel):
        logging.warning("Halting system")
        os.system("sudo reboot")

    def stop_server(self):
        #signal interupt (CTRL+C) which will be caught by our main handler
        logging.warning("Stopping camera program")
        os.kill(os.getpid(), signal.SIGINT)



class RemoteHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ['GET']

    def initialize(self, cfg, cam):
        logging.debug("WebServer RemoteHandler initialize()")
        self.cam = cam # the hawkeypicamera object which contains the picamera object
        self.cfg = cfg
        return

    def get(self, path):
        logging.debug("WebServer RemoteHandler get()")
        refresh = False
        err = self.cam.errcode
        self.cam.errcode = constants.MSGOK

        # get necessary configuration
        command = self.get_query_argument("cmd", "refresh")
        setting = self.get_query_argument("val", None)

        if command == "shutter":
            if self.cam:
                self.cam.take_pictures() # it would be better to virtual trigger the button but we don't have access to that object
            refresh = True
        elif command == "refresh":
            page = constants.WEBROOT + 'remote.html'
            self.render(page)
        else:
            self.cam.errcode = constants.MSGBADCMD
            refresh = True

        if refresh:
            self.redirect("/remote")
        return # tornado.web.HTTPError(404)


class WebcamHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ['GET']

    def get(self, path):
        logging.debug("WebServer WebcamHandler get()")
        # regardless of the resource request, respond with the top level index file
        page = constants.WEBROOT + 'webcam.html'
        self.render(page)
        return


class VideoSocket(tornado.websocket.WebSocketHandler):

    def initialize(self, cam):
        logging.debug("WebServer WebSocket initialize()")
        self.cam = cam # the hawkeypicamera object which contains the picamera object
        self.is_capturing = False
        return

    def on_message(self, msg):
        """Evaluates the function pointed to by json-rpc."""
        parms = msg.split('?')
        if len(parms) < 2:
            message = msg
        else:
            message = parms[0]
        if message == "read_camera":
            if not self.is_capturing:
                self.is_capturing = True

                # poor man's blocking code
                while self.cam.in_use:
                    time.sleep(0.1)
                self.cam.in_use = True

                self.camfile = "{}webcam.jpg".format(constants.FILE_FOLDER)
                size = 0
                # we should not be messing directly with the camera and the following code should be in the printcamera.py file
                if len(parms) > 1:
                    size = int(parms[1])
                    if size == 160:
                        self.cam.camera.resolution = (160, 120)
                    elif size == 320:
                        self.cam.camera.resolution = (320, 240)
                    elif size == 640:
                        self.cam.camera.resolution = (640, 480)
                    elif size == 1280:
                        self.cam.camera.resolution = (1280, 960)
                    else:
                        size = 0
                zoom = 0
                if len(parms) > 2:
                    zoom = int(parms[2])
                    if zoom == 2:
                        self.cam.camera.zoom = (0.25, 0.25, 0.5, 0.5)
                    if zoom == 4:
                        self.cam.camera.zoom = (0.375, 0.375, 0.25, 0.25)
                    if zoom == 8:
                        self.cam.camera.zoom = (0.4375, 0.4375, 0.125, 0.125)
                    else:
                        zoom = 0    
                self.cam.camera.capture(self.camfile, "jpeg", use_video_port=True)
                try:
                    self.write_message("webcam.jpg" + "?" + repr(time.time()))
                except tornado.websocket.WebSocketClosedError:
                    logging.warning("VideoSocket: SocketClosedError")
                # reset zoom and/or resolution if they were changed
                if zoom > 0:
                    self.cam.camera.zoom = (0.0, 0.0, 1.0, 1.0)
                if size > 0:
                    self.cam.camera.resolution = (constants.CAMERA_WIDTH, constants.CAMERA_HEIGHT)
                self.is_capturing = False
                self.cam.in_use = False
            else:
                logging.info("VideoSocket: skipping read_camera()")
        elif message == "take_photo":
            self.cam.take_pictures() # it would be better to virtual trigger the button but we don't have access to that object
        else:
            logging.warning("VideoSocket: Unsupported message: " + message)


class WebServer:
    def __init__(self, cfg, cam):
        logging.debug('entering WebServer __init__()')
        self.errcode = constants.MSGOK

        # get necessary configuration
        self.port = cfg.getint('general', 'webport')

        # setup our web server to serve up the webcontroller and receive websocket requests
        self.http_server = tornado.httpserver.HTTPServer(tornado.web.Application([
            (r'(.*)/$', IndexHandler,),
            (r'/videosocket',VideoSocket, {'cam': cam}),
            (r'/webcam(.*)$', WebcamHandler),
            (r'/bw(.*)$', PrintHandler,),
            (r'/api(.*)$', CommandHandler, {'cfg': cfg, 'cam': cam}),
            (r'/remote(.*)$', RemoteHandler, {'cfg': cfg, 'cam': cam}),
            (r'/(.*)$', StaticFileHandler, {'path': constants.WEBROOT})
        ]))

        self.http_server.listen(self.port)
        logging.debug('exiting WebServer __init__()')
        # return

    def start(self):
        logging.debug('WebServer start()')
        # start the web server as the main loop / thread
        logging.info('Serving HTTP on 0.0.0.0 port {:d}'.format(self.port))
        tornado.ioloop.IOLoop.instance().start()
        return

    def stop(self):
        logging.debug('WebServer stop()')
        tornado.ioloop.IOLoop.instance().stop()
        # sleep(1) # give time for threads to complete
        logging.info('HTTP Server Stopped')
        return
