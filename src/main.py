'''
main.py will setup all of the necessary objects and threads to run the hawkeye pi camera

The general structure of the program runs from several threads
    - one for the shutter button
    - one for the program button
    - one for teh web server
    - one for the shutter LED (use as a wait indicator)
    - one for the program LED (not necessary but it reuses the LEDIndicator class)

The logic of the program is to wait for either a webserver GET command or shutter release.
The shutter release will either immediately take a photo or - if photobooth mode is active - will
blink the program LED as a countdown to take one or more photos.
After the photo(s) taking step, the camera will optionally print if enabled.
During the print process the shutter LED will slow-blink to indicate the camera is busy.

The camera supports a programming mode which is entered by holding the programming
button for >= 3 seconds. When in programming mode, the programming LED will blink the
current mode and the shutter LED will blink the current value.

Without any audio or screen feedback, the programming mode has limited usefulness
because the operator mu memorize the modes and settings. It is possible to print
out a cheat-sheet and stick it to the bottom or back of the camera.

Created on 01-May-2017
@author: bradan lane studio

'''

# cSpell:disable
# pylint: disable=I0011, C0321, C0301, W0613, W1202

import signal
from time import sleep
import logging

# hardware IO
import RPi.GPIO as GPIO


import constants
from configurator import Configurator
from led import LEDIndicator
from button import Button
from webserver import WebServer
from printercamera import HawkeyePiCamera
from programmer import HawkeyePiProgrammer

logging.basicConfig(level=logging.INFO)
#logging.basicConfig(level=logging.DEBUG)

# the following three lines are used for remote debugging
'''
import ptvsd
ptvsd.enable_attach("my_secret", address=('0.0.0.0', 3000))
ptvsd.wait_for_attach()
'''

# -----------------------------------------------------------------------------------------------
# main routine
# -----------------------------------------------------------------------------------------------

def main():
    ''' initialize everything so we can have the inner functions at the start of our main() '''

    cfg = shutter_led = program_led = shutter_button = program_button = picam = webserver = None

    def startup():
        ''' create all objects and start all threads in the correct order '''
        nonlocal shutter_led, program_led, shutter_button, program_button, programmer, picam, webserver
        if shutter_led: shutter_led.start()
        if program_led: program_led.start()
        if shutter_button: shutter_button.start()
        if program_button: program_button.start()
        if programmer: programmer.start()
        if picam: picam.start()
        if webserver: webserver.start()
        return

    def shutdown(signum, frame):
        ''' stop all threads and clean up all objects in the reverse order '''
        nonlocal shutter_led, program_led, shutter_button, program_button, programmer, picam, webserver
        if webserver: webserver.stop()
        if picam: picam.stop()
        if programmer: programmer.stop()
        if program_button: program_button.stop()
        if shutter_button: shutter_button.stop()
        if program_led: program_led.stop()
        if shutter_led: shutter_led.stop()
        GPIO.cleanup() # must do it just just once
        sleep(1) # give a little time for the threads to finish
        return

    # handle CTRL+C and systemd signal events with local function
    signal.signal(signal.SIGINT, shutdown) # needed for CTRL+C
    signal.signal(signal.SIGTERM, shutdown) # needed for systemd


    # initialize configuration
    cfg = Configurator()

    # initialize the LEDs
    shutter_led = LEDIndicator(led=constants.SHUTTER_LED)
    program_led = LEDIndicator(led=constants.FRONT_LED)

    # initialize the camera (which internally initializes the printer)
    picam = HawkeyePiCamera(cfg, shutter_led, program_led)

    programmer = HawkeyePiProgrammer(cfg, picam, program_led, shutter_led)

    shutter_button = Button(button=constants.SHUTTER_BUTTON, bounce=1000, function=programmer.shutter_button)
    program_button = Button(button=constants.PROGRAM_BUTTON, bounce=1000, function=programmer.program_button)

    # initialize the web server (which needs cfg and the camera for the UI actions)
    webserver = WebServer(cfg, picam)

    startup()
    return


#Main
if __name__ == '__main__':
    main()
    print("Exited")
