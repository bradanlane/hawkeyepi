'''
Created on 01-May-2017
@author: bradan lane studio

'''
import logging
from threadclass import ThreadClass
import RPi.GPIO as GPIO

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202, W0201


# -----------------------------------------------------------------------------------------------
# Button class (uses GPIO)
# -----------------------------------------------------------------------------------------------

class Button(ThreadClass):
    def init(self, **kwargs):
        logging.debug("entering Button init()")
        self.gpio_pin = 0
        self.func = None
        bounce = 1000

        if kwargs is not None:
            self.gpio_pin = kwargs.get('button', 0)
            bounce = kwargs.get('bounce', 1000)
            self.func = kwargs.get('function', None)
        if not self.gpio_pin:
            logging.warning("no GPIO pin assigned to button")
        logging.debug("GPIO pin {:02d} assigned to button".format(self.gpio_pin))

        GPIO.setmode(GPIO.BOARD)
        if self.gpio_pin:
            GPIO.setup(self.gpio_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(self.gpio_pin, GPIO.FALLING, callback=self.button_press, bouncetime=bounce)
        logging.debug("exiting Button init()")
        return

    def term(self):
        logging.debug("Button term()")
        self.gpio_pin = 0
        #GPIO.cleanup()
        return

    def work(self):
        logging.debug("entering Button work()")
        # by default, we clear the event which trigger us
        self.trigger(False)
        # then we call to do the real work
        if self.func:
            self.func()
        logging.debug("exiting Button work()")
        return

    def button_press(self, channel):
        # the only thing we want to do is trigger the thread work and return quickly
        self.trigger(True)
        return
