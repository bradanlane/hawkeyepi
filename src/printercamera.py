'''
Created on 01-May-2017
@author: bradan lane studio

'''

# cSpell:disable
# pylint: disable=I0011, C0111, C0321, C0301, W0613, W1202

import os
import glob
from time import sleep
from fractions import Fraction
import datetime
import logging

# hardware IO
from Adafruit_Thermal import *
from PIL import Image
from picamera import PiCamera


import constants


# -----------------------------------------------------------------------------------------------
# a system check
# -----------------------------------------------------------------------------------------------
def init_storage():
    logging.debug("entering init_storage()")

    # sanity check to make sure we have a working storage device
    if not os.listdir(constants.TEST_FOLDER):
        return constants.MSGNOUSB
    else:
        # make sure our storage device has the necessary directory structure
        if not os.path.exists(constants.FILE_FOLDER):
            os.makedirs(constants.FILE_FOLDER)
        if not os.path.exists(constants.THUMB_FOLDER):
            os.makedirs(constants.THUMB_FOLDER)
        if not os.path.exists(constants.PRINT_FOLDER):
            os.makedirs(constants.PRINT_FOLDER)
        # if our directories still do not exist, we flash an error
        if not os.path.exists(constants.THUMB_FOLDER):
            return constants.MSGUSBFOLDERS
    logging.debug("exiting init_storage()")
    return constants.MSGOK



# -----------------------------------------------------------------------------------------------
# our Thermal Printer class
# -----------------------------------------------------------------------------------------------

class HawkeyePiPrinter:
    def __init__(self, cfg, led=None):
        logging.debug("entering HawkeyePiPrinter __init__")
        self.errcode = constants.MSGOK
        self.cfg = cfg
        self.indicator = led
        self.hdwr = None
        self.has_paper = False

        # get necessary configuration
        baudrate = cfg.getint('printer', 'baudrate')
        dotprintspeedadjust = cfg.getfloat('printer', 'dotprinttime')
        print_darkness = cfg.getint('printer', 'darkness')

        if baudrate > 1:
            logging.debug("HawkeyePiPrinter initialize serial0 with baudrate={:d}".format(baudrate))
            self.hdwr = Adafruit_Thermal("/dev/serial0", baudrate, timeout=5)

        if not self.hdwr:
            self.errcode = constants.MSPRINTCREATE
            logging.debug("unable to create serial printer")
        else:
            logging.debug("printer dot speed  = {}".format(self.hdwr.dotPrintTime))
            if dotprintspeedadjust > 0:
                self.hdwr.dotPrintTime = dotprintspeedadjust
                logging.debug("adjusted dot speed = {}".format(dotprintspeedadjust))
            self.hdwr.begin(print_darkness)
            self.sleep()
            logging.debug("Printer initialization complete")

            if not self.errcode:
                self.ready = True
        logging.debug("exiting HawkeyePiPrinter __init__")
        # return


    def wake(self):
        logging.debug("wake printer")
        # get necessary configuration
        if self.hdwr:
            self.hdwr.online()
            self.hdwr.wake()
        return


    def sleep(self):
        logging.debug("sleep printer")
        # get necessary configuration
        if self.hdwr:
            self.hdwr.sleep() # sleepAfter(5) # give it time to stop printing
            self.hdwr.offline()
        return

    def print_photo(self, shortname):
        logging.debug("entering HawkeyePiPrinter print_photo()")
        # print photo even if you has toggled printer when taking photos
        if not self.hdwr: return

        self.wake()
        if self.indicator:
            self.indicator.trigger(True) # start blinking the LED to indicate we are busy

        filename = "{}{}".format(constants.FILE_FOLDER, shortname)
        # sanity check - make sure we have been initialized to something useful
        logging.debug("creating thumbnail for {}".format(filename))

        # resize to printer resolution and send to printer
        try:
            #open original image
            image = Image.open(filename)
            im_width, im_height = image.size
            if im_width > im_height:
                logging.debug("creating rotated print for {}".format(filename))
                 # by scaling first, the rotate should be faster
                image.thumbnail((constants.PRINTER_WIDTH, constants.PRINTER_HEIGHT), Image.ANTIALIAS)
                image = image.rotate(90)
            else:
                image.thumbnail((constants.PRINTER_HEIGHT, constants.PRINTER_WIDTH), Image.ANTIALIAS)
            #go ahead and print the scaled image
            logging.debug("printing thumbnail for {}".format(filename))
            logging.debug("printer buffering values are dotPrintTime={} and dotFeedTime={} resulting in buffering timeout={}".format(self.hdwr.dotPrintTime, self.hdwr.dotFeedTime, 255.0*self.hdwr.dotPrintTime))
            self.hdwr.printImage(image, False) # last arg is for line-at-a-time printing
            self.hdwr.flush()
            logging.debug("printed thumbnail for {}".format(filename))
        except IOError:
            logging.warning("cannot print image file {}".format(filename))

        self.hdwr.feed(3)

        self.sleep()
        if self.indicator:
            self.indicator.trigger(False) # stop blinking the LED to indicate we are done

        logging.debug("exiting HawkeyePiPrinter print_photo()")
        return


    def process_new_photos(self, index, maxcount):
        logging.debug("entering HawkeyePiPrinter process_new_photos()")
        # print most recent 'burstsize' photos
        if not self.ready: return

        # get necessary configuration
        printer_on = self.cfg.getboolean("general", "print")
        tagline = str(self.cfg.get('printer', 'tagline'))

        if printer_on:
            self.wake()

        if self.indicator:
            self.indicator.trigger(True) # start blinking the LED to indicate we are busy

        # sanity check - make sure we have been initialized to something useful
        if index < 1:
            logging.warning("no new photos to print")
            return

        for count in range(0, maxcount):
            if maxcount > 1:
                shortname = "img_{:05d}-{:d}.jpg".format(index, count+1)
            else:
                shortname = "img_{:05d}.jpg".format(index)
            filename = "{}{}".format(constants.FILE_FOLDER, shortname)
            logging.debug("creating thumbnail for {}".format(shortname))

            # resize to printer resolution and send to printer
            try:
                #open original image
                image = Image.open(filename)
                #create thumbnail and save for web display
                image.thumbnail((constants.PRINTER_HEIGHT, constants.PRINTER_WIDTH), Image.ANTIALIAS)
                image.save("{}{}".format(constants.THUMB_FOLDER, shortname), "JPEG")

                image = image.convert(mode='1')
                image.save("{}{}".format(constants.PRINT_FOLDER, shortname), "JPEG")

                if printer_on:
                    #if we are printing a single photo, we may need to rotate it to make it bigger
                    if not maxcount > 1:
                        im_width, im_height = image.size
                        if im_width > im_height:
                            logging.debug("creating rotated print {}".format(filename))
                            image = Image.open(filename)
                            image = image.rotate(90)
                            image.thumbnail((constants.PRINTER_HEIGHT, constants.PRINTER_WIDTH), Image.ANTIALIAS)
                    #go ahead and print the scaled image
                    logging.debug("printing thumbnail for {}".format(filename))
                    logging.debug("printer buffering values are dotPrintTime={} and dotFeedTime={} resulting in buffering timeout={}".format(self.hdwr.dotPrintTime, self.hdwr.dotFeedTime, 255.0*self.hdwr.dotPrintTime))
                    if not count: # on teh first pass, we start teh clock as soon as we start to print
                        start_timer = time.time() # track how long it takes to run through this code
                    self.hdwr.printImage(image, False) # last arg is for line-at-a-time printing
                    self.hdwr.flush()
                    logging.debug("printed thumbnail for {}".format(filename))

                    # code you want to evaluate
                    elapsed = int(round(time.time() - start_timer))
                    # we wait a little bit to let the printer catch up between prints
                    # if we wasted time rotating a subsequent image, we are deducting it from the delay
                    if (elapsed < 10) and (count < maxcount):
                        logging.debug("waiting {:02d} seconds to let the current print finish".format(10-elapsed))
                        sleep(10-elapsed)
                        if (count + 1) < maxcount:
                            #self.hdwr.feedRows(20) # fee 20 vertical pixels
                            self.hdwr.feed(1)
                            logging.debug("printed linebreak between multiple photos")
                        start_timer = time.time() # reset the clock
                # end if printer_on
            except IOError:
                logging.warning("cannot print image file {}".format(filename))


        if printer_on:
            if tagline:
                #add a tag line
                sleep(0.25)
                self.hdwr.justify('C')
                self.hdwr.setSize('M')
                today = datetime.date.today()
                tagline = tagline.replace("{date}", today.strftime("%b %d, %Y"))
                tagline = tagline.replace("{shortdate}", today.strftime("%b %d"))
                logging.debug("printing tagline '{}'".format(tagline))
                self.hdwr.println(tagline)
            #feed the paper for easy cutting
            #self.hdwr.feedRows(80) # fee 20 vertical pixels
            self.hdwr.feed(3)

        if self.indicator:
            self.indicator.trigger(False) # stop blinking the LED to indicate we are done

        if printer_on:
            self.sleep()

        logging.debug("exiting HawkeyePiPrinter process_new_photos()")
        return





# -----------------------------------------------------------------------------------------------
# our PolaPiCamera class
# -----------------------------------------------------------------------------------------------

class HawkeyePiCamera:
    def __init__(self, cfg, shutter_led, flash_led):
        logging.debug("entering HawkeyePiCamera __init__()")
        # perform all necessary setup

        self.ready = False
        self.in_use = False
        self.errcode = constants.MSGOK
        self.current_photo_index = -1
        self.cfg = cfg
        self.indicator = shutter_led
        self.flash = flash_led

        # Create camera
        self.camera = PiCamera(sensor_mode=2)

        # config camera
        if not self.camera:
            self.errcode = constants.MSGCAMCREATE
        else:
            self.errcode = init_storage()

            # save camera constants for others to use
            constants.AWB_MODES = self.camera.AWB_MODES
            constants.METER_MODES = self.camera.METER_MODES
            constants.EXPOSURE_MODES = self.camera.EXPOSURE_MODES

            self.camera.resolution = (constants.CAMERA_WIDTH, constants.CAMERA_HEIGHT)
            self.camera.framerate = 15 # this is our default and means our slowest shutter is 1/15 second
            self.camera.iso = 400
            self.camera.still_stats = True # speeds up photo taking by eliminating some auto sensing
            self.load_config()

            # make any system changes based on loaded configuration
            if self.camera.exposure_mode == "night":
                # this really stretches the time to take a photo
                self.camera.framerate = Fraction(1, 6) # this means our slowest shutter is 6 seconds

        self.printer = HawkeyePiPrinter(cfg, self.indicator)
        logging.debug("exiting HawkeyePiCamera __init__()")
    # return


    def load_config(self):
        logging.debug("entering HawkeyePiCamera load_config()")
        # get necessary configuration
        self.camera.iso = self.cfg.getint('camera', 'iso')
        self.camera.contrast = self.cfg.getint('camera', 'contrast')
        self.camera.brightness = self.cfg.getint('camera', 'brightness')
        awb_red = self.cfg.getfloat('camera', 'awb_red')
        awb_blue = self.cfg.getfloat('camera', 'awb_blue')
        self.camera.awb_gains = (awb_red, awb_blue)
        self.camera.awb_mode = self.cfg.get('camera', 'awb')
        self.camera.exposure_mode = self.cfg.get('camera', 'exposure')
        self.camera.meter_mode = self.cfg.get('camera', 'metering')
        speed = self.cfg.getint('camera', 'speed')
        if speed < 0:
            sleep = 0
        elif speed > 0:
            speed = round (1000000 / speed)
        self.camera.shutter_speed = speed
        self.camera.rotation = self.cfg.getint('camera', 'rotation')
        self.camera.hflip = self.cfg.getboolean('camera', 'hflip')
        self.camera.vflip = self.cfg.getboolean('camera', 'vflip')
        logging.debug("exiting HawkeyePiCamera load_config()")
        return


    def start(self):
        logging.debug("entering HawkeyePiCamera start())")
        if self.camera:
            self.ready = True
        # turn on the shutter button to indicate we are ready
        if self.indicator: self.indicator.enable()
        constants.PlayAudioMessage(constants.AUDIO_READY, True)
        logging.info("ready for pictures")
        logging.debug("exiting HawkeyePiCamera start())")
        return


    def stop(self):
        logging.debug("entering HawkeyePiCamera stop())")
        logging.info("no longer ready for pictures")
        self.ready = False
        if self.indicator: self.indicator.disable()
        #sleep(1) # give everyone time to shutdown cleanly
        logging.debug("exiting HawkeyePiCamera stop())")
        return


    def __next_available_index(self):
        if self.current_photo_index < 0:
            i = 1
        else:
            i = self.current_photo_index
        while True:
            # we want to detect any image files using the index
            # this will match individual files and any burst files
            # eg: img_00000.jpg as well as img_00000-1.jpg
            test_name = '{}img_{:05d}*.jpg'.format(constants.FILE_FOLDER, i)
            if next(glob.iglob(test_name), False):
                i += 1
            else:
                break
        self.current_photo_index = i
        return i



    def delete_photo(self, shortname):
        filename = "{}{}".format(constants.FILE_FOLDER, shortname)
        try:
            os.remove(filename)
        except OSError:
            pass
        logging.debug("deleted file {}".format(filename))
        filename = "{}{}".format(constants.THUMB_FOLDER, shortname)
        try:
            os.remove(filename)
        except OSError:
            pass
        logging.debug("deleted file {}".format(filename))
        filename = "{}{}".format(constants.PRINT_FOLDER, shortname)
        try:
            os.remove(filename)
        except OSError:
            pass
        logging.debug("deleted file {}".format(filename))
        return


    def take_pictures(self):
        logging.debug("entering HawkeyePiCamera take_pictures()")
        # prepare to take a picture (or burst pictures)
        if not self.ready:
            logging.debug("oops, camera not ready")
            return

        # poor man's blocking code
        while self.in_use:
            sleep(0.1)
        self.in_use = True

        if self.indicator: self.indicator.disable()

        index = self.__next_available_index()

        # get necessary configuration
        booth_mode = self.cfg.getboolean("general", "photobooth")
        if booth_mode:
            maxcount = self.cfg.getint("general", "burstsize")
        else:
            maxcount = 1
        booth_delay = self.cfg.getint("general", "delay")

        shutter_sound = self.cfg.getboolean("general", "audio")

        # consider taking three pictures - five seconds apart - and then printing the series
        # since we need to name each, we cannot use camera.capture_continuous()

        # since the camera is always active, we could just grab an image immediately
        # but when we are simulating a photo booth, we want to give the user time to prepare

        for count in range(0, maxcount):
            if maxcount > 1:
                filename = "{}img_{:05d}-{:d}.jpg".format(constants.FILE_FOLDER, index, count+1)
            else:
                filename = "{}img_{:05d}.jpg".format(constants.FILE_FOLDER, index)
            logging.debug("capturing to {}".format(filename))

            if booth_mode:
                # full 'delay' before first photo, then at most 5 seconds delay between subsequent photos
                # blink LED each second and then 200ms to create 5 quick flashes for last second
                delay = booth_delay
                if count > 0:
                    delay = min(5, booth_delay)
                if (delay > 0) and self.flash:
                    self.flash.countdown(delay)

            # capture the photo
            if shutter_sound:
                constants.PlayAudioMessage(constants.AUDIO_SHUTTER, False)
            self.camera.capture(filename)
            logging.debug("captured to {}".format(filename))

        # print the photo - use use the index and count in case we captured multiple images
        self.printer.process_new_photos(index, maxcount)
        # finished taking picture; get ready for the next one
        if self.indicator: self.indicator.enable()
        logging.info("ready for more pictures")
        logging.debug("exiting HawkeyePiCamera take_pictures()")
        self.in_use = False
        return

