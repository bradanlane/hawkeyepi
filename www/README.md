The web interface is not meant as an example of proper mobile web design - for that it would use be a single page UI and utilize AJAX and a socket layer to the Python Tornado server. Rather, this is a temporary solution as the audio menus interface evolves.

All of the most common adminstrative settings have been added to the audio menu system. The numeric value settings - such as changign brightness, contrast, timer delay, and print darkness - have not been designed.

This web interface is a simple HTTP GET API with key and value.