
/* global $, WebSocket, console, window, document */
"use strict";

/* Connects to Pi server and receives video data. */
var client = {
    // Connects to Pi via websocket
    is_started: false,
    is_fetching: false,
    image_size: 320,
    image_zoom: 0,

    connect: function (port) {
        var video = document.getElementById("video"),
            message = document.getElementById("message");

        this.is_fetching = false;
        this.socket = new WebSocket("ws://" + window.location.hostname + ":" + window.location.port + "/videosocket");

        video.onload = function () {
            // once the latest image has loaded, we are ready for another frame
            client.is_fetching = false;
        };

        // Request the video stream once connected
        this.socket.onopen = function () {
            message.innerHTML = "";
            this.is_started = true;
            window.setTimeout(request_frame, 20);
        };

        // Currently, all returned messages are video data. However, this is
        // extensible with full-spec JSON-RPC.
        this.socket.onmessage = function (messageEvent) {
            //video.src = "data:image/jpeg;base64," + messageEvent.data;
            video.src = "photos/" + messageEvent.data;;
        };
    },

    setSize: function (size) {
        this.image_size = size;
    },

    setZoom: function (zoom) {
        this.image_zoom = zoom;
    },

    getFrame: function () {
        this.socket.send("read_camera" + "?" + this.image_size + "?" + this.image_zoom);
    },
    saveImage: function () {
        this.socket.send("take_photo");
    }
};

function request_frame() {
    if (!client.is_started) {
        if (!client.is_fetching) {
            // prevent piling up requests - this allows the webcam to operate as fast as possible and as slow as needed
            client.is_fetching = true;
            client.getFrame();
        }
        window.setTimeout(request_frame, 20);
    }
}

function stop_requests() {
    client.is_started = false;
}