The Hawkeye Pi Camera is yet another Raspberry Pi Camera with a cheap thermal printer.

The project pulls ideas and code snippets from a number of sources:

* The original concept came from Pierre Muth’s [PolaPi-Zero](https://hackaday.io/project/19731-polapi-zero) project.
* The read-only configuration of the Raspberry Pi’s SD card came from Pascal Suter’s [post](http://wiki.psuter.ch/doku.php?id=solve_raspbian_sd_card_corruption_issues_with_read-only_mounted_root_partition) he wrote to solve a corruption issue.
* The integrated hotspot came from Roboberry’s [post](http://www.raspberryconnect.com/item/320-rpi3-auto-wifi-hotspot-if-no-internet-oldscript) and his [other post](http://www.raspberryconnect.com/network/item/327-raspberry-pi-auto-wifi-hotspot-switch).
* The systemd service description came from Matt and his Raspberry Pi Spy [tutorial](http://www.raspberrypi-spy.co.uk/2015/10/how-to-autorun-a-python-script-on-boot-using-systemd/).
* The integrated webserver came from past projects including the LoCoRo (low cost robot) project.

The video overview is at: https://vimeo.com/218526579
