*this is a work in progress and represents some basic notes on how the camera system level setup is achieved*



Command: `sudo systemctl disable dhcpcd.service` - this will eliminate the 30 second timeout waiting for eth0.
It does not appear to affect network operations.

Follow the latest instructions here: http://www.raspberryconnect.com/network/item/327-raspberry-pi-auto-wifi-hotspot-switch
Command: `sudo cp autohotspotswapper /usr/bin/autohotspotswapper` and `sudo chmod +X /usr/bin/autohotspotswapper`
Command: `sudo cp autohotspot.service /lib/systemd/system/autohotspot.service`
Command: `sudo systemctl enable autohotspot.service`
These setup the networking used for development and for the mobile web interface

**IMPORTANT:** Remember to edit the `cmdline.txt.*` files to change the `PARTUUID` to match your machine.